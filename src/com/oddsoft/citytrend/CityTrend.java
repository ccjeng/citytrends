package com.oddsoft.citytrend;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;

import java.util.ArrayList;
import java.util.Collections;

import com.mobfox.sdk.BannerListener;
import com.mobfox.sdk.MobFoxView;
import com.mobfox.sdk.RequestException;
import com.oddsoft.citytrend.twitter.TrendAvailableAdapter;
import com.oddsoft.citytrend.twitter.TrendAvailableItem;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class CityTrend extends Activity implements BannerListener {
	private static final String TAG = "CityTrend";
	private ListView trendavlListView;
	private ArrayList<TrendAvailableItem> list;
	private ProgressDialog dialog;
	GoogleAnalyticsTracker tracker;
	private MobFoxView mobfoxView;
	final Handler updateHandler = new Handler();
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tracker = GoogleAnalyticsTracker.getInstance();
        tracker.start("UA-19743390-4", this);
        tracker.trackPageView("/CityTrends");
        setContentView(R.layout.main);
                trendavlListView = (ListView)this.findViewById(R.id.ListViewAvailable);
        ArrayAdapter<TrendAvailableItem> aa = null;
        TrendAvailableAdapter trendAvailable = new TrendAvailableAdapter();
        
        list = trendAvailable.getData();
        //sort
        Collections.sort(list);
        
	    aa = new ArrayAdapter<TrendAvailableItem>(this, R.layout.listitem, list);        
	    trendavlListView.setAdapter(aa);
	    trendavlListView.setSelection(0);
	    trendavlListView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> av, View view, int index, long arg3) {
            	goIntent(list.get(index).getWoeid().toString(), //woeid
            			list.get(index).toString()); //name
            	//Log.d(TAG, list.get(index).getWoeid().toString());
                
			}
        });
	    adView();
    }
    
    private void goIntent(String woeid, String name) {
    	showDialog(0);
    	Intent intent = new Intent();
        intent.setClass(CityTrend.this, LocationActivity.class);
        Bundle bundle = new Bundle();
        
        bundle.putString("WOEID", woeid.toString());
        bundle.putString("NAME", name.toString());
        Log.d(TAG, "WOEID=" + woeid.toString());
        Log.d(TAG, "NAME=" + name.toString());
        tracker.trackEvent("Clicks","Location",name.toString(),0);
        tracker.dispatch();

        intent.putExtras(bundle);
        startActivityForResult(intent,0);
    }
    
    @Override
    protected void onDestroy() {
       super.onDestroy();
       // Stop the tracker when it is no longer needed.
       tracker.dispatch();
       tracker.stop();
    }
    
	@Override
	protected void onPause() {
		super.onPause();
		mobfoxView.pause();
		if (dialog != null)
			dialog.dismiss();
	}
	@Override
	protected void onResume() {
		super.onResume();
		mobfoxView.resume();
	} 	
    @Override
    protected Dialog onCreateDialog(int id) {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        return dialog;
    }
    
	private void adView() {
   	    String publisherID = "fde350e2b6523e36a012441056e36409";
	    boolean includeLocation = true;
	    boolean animation = true;
	   	LinearLayout linearLayout = (LinearLayout) findViewById(R.id.Linear_Main);
	   	int diWidth = 320;
   		int diHeight = 52;
   		float density = getResources().getDisplayMetrics().density;
   		mobfoxView = new MobFoxView(this, publisherID, includeLocation, animation);
   		mobfoxView.setBannerListener((BannerListener) this);
		mobfoxView.setVisibility(View.GONE);
		linearLayout.addView(mobfoxView
				, (int)(diWidth * density)
				, (int)(diHeight * density));		
	}

	//For MobFoxView
	@Override
	public void bannerLoadFailed(RequestException arg0) {
		Log.d(TAG, "MobFox - Error loading ad");
	}
	
	@Override
	public void bannerLoadSucceeded() {
		updateHandler.post(new Runnable() {
			public void run() {
				mobfoxView.setVisibility(View.VISIBLE);
				Log.d(TAG, "MobFox - Ad loaded successfully");
			}
		});
	}
	
	@Override
	public void noAdFound() {
		Log.d(TAG, "MobFox - no Ad Found");
	}

}