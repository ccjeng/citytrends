package com.oddsoft.citytrend;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mobfox.sdk.BannerListener;
import com.mobfox.sdk.MobFoxView;
import com.mobfox.sdk.RequestException;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.LinearLayout;

public class WebviewActivity extends Activity implements BannerListener {
	private static final String TAG = "WebviewActivity";
	private WebView webview;
	GoogleAnalyticsTracker tracker;
	private MobFoxView mobfoxView;
	final Handler updateHandler = new Handler();
	
	public void onCreate(Bundle savedInstanceState) {
	       super.onCreate(savedInstanceState);
	       tracker = GoogleAnalyticsTracker.getInstance();
	       tracker.start("UA-19743390-4", this);
	       tracker.trackPageView("/WebView");
	       // Adds Progrss bar Support
		   getWindow().requestFeature(Window.FEATURE_PROGRESS);
	       setContentView(R.layout.web);
	       webview = (WebView) findViewById(R.id.webview); 
	       
	       adView();
	    	
	       Bundle bunde = this.getIntent().getExtras();
	       int searchEngine = bunde.getInt("SearchEngine");
	       String searchURL = bunde.getString("URL");
	       String url = "";
	       switch (searchEngine) {
		   /*    case 0: //Twitter
		    	   url = "http://mobile.twitter.com/search/";
					break;*/
		       case 0: //Google
		    	   url = "http://www.google.com/m/search?source=Android-unknown&q=";
					break;
		       case 1: //Bing
		    	   url = "http://m.bing.com/search/search.aspx?A=webresults&Q=";
					break;
	       }
	       
	       url = url + searchURL.replace(" ", "+").toString();
	       Log.d(TAG, url.toString());
	       tracker.dispatch();
	       
		   // Makes Progress bar Visible
		   getWindow().setFeatureInt( Window.FEATURE_PROGRESS, Window.PROGRESS_VISIBILITY_ON);
			
	       webview.getSettings().setJavaScriptEnabled(true);
	   	   //Zoom Control on web
	       webview.getSettings().setSupportZoom(true); 
           //if ROM supports Multi-Touch      
	       webview.getSettings().setBuiltInZoomControls(true); 
		   webview.loadUrl(url.toString());
		   
		 //progress bar
		   final Activity wvActivity = this;
		   webview.setWebChromeClient(new WebChromeClient() {
	            public void onProgressChanged(WebView view, int progress)   
	            {
	             //Make the bar disappear after URL is loaded, and changes string to Loading...
	            	wvActivity.setProgress(progress * 100); //Make the bar disappear after URL is loaded
	             }
	            });
	}
	
    @Override
    protected void onDestroy() {
       super.onDestroy();
       // Stop the tracker when it is no longer needed.
       tracker.dispatch();
       tracker.stop();
    }
	@Override
	protected void onPause() {
		super.onPause();
		mobfoxView.pause();
	}
	@Override
	protected void onResume() {
		super.onResume();
		mobfoxView.resume();
	} 		

	private void adView() {
   	    String publisherID = "fde350e2b6523e36a012441056e36409";
	    boolean includeLocation = true;
	    boolean animation = true;
	   	LinearLayout linearLayout = (LinearLayout) findViewById(R.id.Linear_Web);
	   	int diWidth = 320;
   		int diHeight = 52;
   		float density = getResources().getDisplayMetrics().density;
   		mobfoxView = new MobFoxView(this, publisherID, includeLocation, animation);
   		mobfoxView.setBannerListener((BannerListener) this);
		mobfoxView.setVisibility(View.GONE);
		linearLayout.addView(mobfoxView
				, (int)(diWidth * density)
				, (int)(diHeight * density));		
	}

	//For MobFoxView
	@Override
	public void bannerLoadFailed(RequestException arg0) {
		Log.d(TAG, "MobFox - Error loading ad");
	}
	
	@Override
	public void bannerLoadSucceeded() {
		updateHandler.post(new Runnable() {
			public void run() {
				mobfoxView.setVisibility(View.VISIBLE);
				Log.d(TAG, "MobFox - Ad loaded successfully");
			}
		});
	}
	
	@Override
	public void noAdFound() {
		Log.d(TAG, "MobFox - no Ad Found");
	}	
}
