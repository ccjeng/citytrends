package com.oddsoft.citytrend;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import java.util.ArrayList;

import com.mobfox.sdk.BannerListener;
import com.mobfox.sdk.MobFoxView;
import com.mobfox.sdk.RequestException;
import com.oddsoft.citytrend.twitter.TrendLocationAdapter;
import com.oddsoft.citytrend.twitter.TrendLocationItem;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class LocationActivity extends Activity implements BannerListener {
	private static final String TAG = "LocationActivity";
	private ListView trendlocListView;
	private TextView nameTextView;
	private ArrayList<TrendLocationItem> list;
	private ProgressDialog dialog;

	GoogleAnalyticsTracker tracker;
	private MobFoxView mobfoxView;
	final Handler updateHandler = new Handler();
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tracker = GoogleAnalyticsTracker.getInstance();
        tracker.start("UA-19743390-4", this);
        tracker.trackPageView("/LocationTrends");
        setContentView(R.layout.location);
        trendlocListView = (ListView)this.findViewById(R.id.ListViewLocationTrend);
        nameTextView = (TextView) findViewById(R.id.name_text);
        
        ArrayAdapter<TrendLocationItem> aa = null;

        //get intent values
		Bundle bunde = this.getIntent().getExtras();
        String woeid = bunde.getString("WOEID");
        String name = bunde.getString("NAME");
        
        nameTextView.setText(getString(R.string.trend_title) + " " + name.toString());
        TrendLocationAdapter trendLocation = new TrendLocationAdapter(woeid.toString());
        
        list = trendLocation.getTrend();
        //sort
        //Collections.sort(list);
        
	    aa = new ArrayAdapter<TrendLocationItem>(this, R.layout.listitem, list);        
	    trendlocListView.setAdapter(aa);
	    trendlocListView.setSelection(0); 
	    registerForContextMenu(trendlocListView);
	    
	    trendlocListView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> av, View view, int index, long arg3) {
            	Log.d(TAG, list.get(index).getUrl().toString());
            	goBrowser(list.get(index).getQuery().toString(),0);           		
			}
        });	    
	    adView();
    }

	private void goBrowser(String query, int menuItemIndex) {
    	showDialog(0);
    	Intent intent = new Intent();
        intent.setClass(this, WebviewActivity.class);
        
        Bundle bundle = new Bundle();
        bundle.putInt("SearchEngine", menuItemIndex);
        bundle.putString("URL", query.toString());
        Log.d(TAG, query.toString());
        tracker.trackEvent("Clicks","Browser",query.toString(),0);
        tracker.dispatch();

        intent.putExtras(bundle);
        startActivityForResult(intent,0); 
    }
    
    @SuppressWarnings("unused")
	@Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
      if (v.getId()==R.id.ListViewLocationTrend) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
        menu.setHeaderTitle("Search in");
        String[] menuItems = getResources().getStringArray(R.array.menu);
        for (int i = 0; i<menuItems.length; i++) {
          menu.add(Menu.NONE, i, i, menuItems[i]);
        }
      }
    }
    
    @Override
    public boolean onContextItemSelected(MenuItem item) {
	    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
	    int menuItemIndex = item.getItemId();
	    Log.d(TAG, "menuItemIndex =" + menuItemIndex);
	    Log.d(TAG, "query =" + list.get(info.position).getQuery().toString());
	    goBrowser(list.get(info.position).getQuery().toString(), menuItemIndex);
	    
	    return true;
    }
    
    @Override
    protected Dialog onCreateDialog(int id) {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        return dialog;
    }
    
    @Override
    protected void onDestroy() {
       super.onDestroy();
       // Stop the tracker when it is no longer needed.
       tracker.dispatch();
       tracker.stop();
    }
    
	@Override
	protected void onPause() {
		super.onPause();
		mobfoxView.pause();
		if (dialog != null)
			dialog.dismiss();	
	}
	@Override
	protected void onResume() {
		super.onResume();
		mobfoxView.resume();
	} 	
    
    private void adView() {
   	    String publisherID = "fde350e2b6523e36a012441056e36409";
	    boolean includeLocation = true;
	    boolean animation = true;
	   	LinearLayout linearLayout = (LinearLayout) findViewById(R.id.Linear_Location);
	   	int diWidth = 320;
   		int diHeight = 52;
   		float density = getResources().getDisplayMetrics().density;
   		mobfoxView = new MobFoxView(this, publisherID, includeLocation, animation);
   		mobfoxView.setBannerListener((BannerListener) this);
		mobfoxView.setVisibility(View.GONE);
		linearLayout.addView(mobfoxView
				, (int)(diWidth * density)
				, (int)(diHeight * density));			
	}

	//For MobFoxView
	@Override
	public void bannerLoadFailed(RequestException arg0) {
		Log.d(TAG, "MobFox - Error loading ad");
	}
	
	@Override
	public void bannerLoadSucceeded() {
		updateHandler.post(new Runnable() {
			public void run() {
				mobfoxView.setVisibility(View.VISIBLE);
				Log.d(TAG, "MobFox - Ad loaded successfully");
			}
		});
	}
	
	@Override
	public void noAdFound() {
		Log.d(TAG, "MobFox - no Ad Found");
	}    
}
