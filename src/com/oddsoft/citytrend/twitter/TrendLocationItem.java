package com.oddsoft.citytrend.twitter;

public class TrendLocationItem implements Comparable<TrendLocationItem> {

	private String url;
	private String query;
	private String name;

	public TrendLocationItem(String url, String query, String name) {
		this.url = url;
		this.query = query;
		this.name = name;
	}
	
	@Override
	public String toString() {

		String result = getName();
		return result;
	}
    @Override
    public int compareTo(TrendLocationItem fi) {
        return this.toString().compareToIgnoreCase(fi.toString());
    }
    
	public String getName() {
		return name;
	}
	public String getQuery() {
		return query;
	}
	public String getUrl() {
		return url;
	}
	
}