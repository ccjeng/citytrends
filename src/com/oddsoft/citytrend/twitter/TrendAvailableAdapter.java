package com.oddsoft.citytrend.twitter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class TrendAvailableAdapter {
	private static final String TAG = "TrendAvailableAdapter";
	
	public ArrayList<TrendAvailableItem> getData() {
		HttpURLConnection con = null;
		ArrayList<TrendAvailableItem> listItems = new ArrayList<TrendAvailableItem>();
		
		try {
			// Build RESTful trend location API
			//http://api.twitter.com/1/trends/available.json
			String link = "http://api.twitter.com/1/trends/available.json";

			Log.d(TAG, link);
			
			URL url = new URL(link);
			con = (HttpURLConnection) url.openConnection();
			con.setReadTimeout(30000 /* milliseconds */);
			con.setConnectTimeout(45000 /* milliseconds */);
		
			con.setRequestMethod("GET");
			con.setDoInput(true);
			
			// Start the query
			con.connect();
			
			 if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
				// Read results from the query
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(con.getInputStream(),"UTF-8"));
				String payload = reader.readLine();
	
				Log.d(TAG, "payload ->" + payload);
				reader.close();	
				
				TrendAvailableItem item = null;
				if (payload != null) {					
					// Load the requested page converted to a string into a JSONObject.
					JSONArray jsonResponse = new JSONArray(payload);
				
					for (int i = 0; i < jsonResponse.length(); i++) {
						JSONObject jo = (JSONObject) jsonResponse.getJSONObject(i);
						//Log.d(TAG, "country= " + jo.getString("country"));
						//Log.d(TAG, "woeid= " + jo.getString("woeid"));
						//Log.d(TAG, "name= " + jo.getString("name"));
						
						item = new TrendAvailableItem(jo.getString("country")
									,jo.getString("woeid")
									,jo.getString("name")
								);
						listItems.add(item);						
					}
				}					
			 } else {
				 //HTTP error

			 }
			
	        
		} catch (IOException e) {
			Log.e(TAG, "IOException: ", e);
			e.printStackTrace();
		} catch (JSONException e) {
			Log.e(TAG, "JSONException: ", e);
			e.printStackTrace();
		}
	
		finally {
			if (con != null) {
			con.disconnect();
			}
		}
		// All done
		//Log.d(TAG, " -> returned " + result);
		return listItems;
	}
}
