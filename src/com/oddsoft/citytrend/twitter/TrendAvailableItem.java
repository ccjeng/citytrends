package com.oddsoft.citytrend.twitter;

public class TrendAvailableItem implements Comparable<TrendAvailableItem> {

	private String country;
	private String woeid;
	private String name;

	public TrendAvailableItem(String country, String woeid, String name) {
		this.country = country;
		this.woeid = woeid;
		this.name = name;
	}
	
	@Override
	public String toString() {		
		String result = null;
		if (getCountry().toString().equals(getName().toString()) 
				|| getCountry().toString().equals("")) {
			result = getName();
		} else {
			result = getCountry() + " - " + getName();
		}		
		return result;
	}

    @Override
    public int compareTo(TrendAvailableItem fi) {
        return this.toString().compareToIgnoreCase(fi.toString());
    }
    
	public String getName() {
		return name;
	}
	public String getWoeid() {
		return woeid;
	}
	public String getCountry() {
		return country;
	}
	
}
