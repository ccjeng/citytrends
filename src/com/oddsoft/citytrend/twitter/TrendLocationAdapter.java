package com.oddsoft.citytrend.twitter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class TrendLocationAdapter {
	private static final String TAG = "TrendLocationHandler";
	private final String woeid;
	
	public TrendLocationAdapter(String woeid) {
		this.woeid=woeid;
	}
	
	public ArrayList<TrendLocationItem> getTrend() {
		HttpURLConnection con = null;
		ArrayList<TrendLocationItem> listItems = new ArrayList<TrendLocationItem>();
		
		try {
			// Build RESTful trend location API
			//http://api.twitter.com/1/trends/1.json
			String link = "http://api.twitter.com/1/trends/" +
				woeid + ".json";

			Log.d(TAG, link);
			
			URL url = new URL(link);
			con = (HttpURLConnection) url.openConnection();
			con.setReadTimeout(30000 /* milliseconds */);
			con.setConnectTimeout(45000 /* milliseconds */);
		
			con.setRequestMethod("GET");
			con.setDoInput(true);
			
			// Start the query
			con.connect();
			
			 if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
				// Read results from the query
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(con.getInputStream(),"UTF-8"));
				String payload = reader.readLine();
	
				Log.d(TAG, "payload ->" + payload);
				reader.close();	
				
				TrendLocationItem item = null;
				if (payload != null) {					
					// Load the requested page converted to a string into a JSONObject.
					JSONArray jsonResponse = new JSONArray(payload);
					// Get the query value
					JSONObject  query = jsonResponse.getJSONObject(0);
					Log.d(TAG, "query= " + query.getString("created_at"));
									
					JSONArray trends = query.getJSONArray("trends");
					
					for (int i = 0; i < trends.length(); i++) {
						JSONObject jo = (JSONObject) trends.getJSONObject(i);
						//Log.d(TAG, "url= " + jo.getString("url"));
						//Log.d(TAG, "query= " + jo.getString("query"));
						//Log.d(TAG, "name= " + jo.getString("name"));
						
						item = new TrendLocationItem(jo.getString("url")
									,jo.getString("query")
									,jo.getString("name")
								);
						listItems.add(item);						
					}
				}
			 } else {
				 //HTTP error

			 }
			     
		} catch (IOException e) {
			Log.e(TAG, "IOException: ", e);
			e.printStackTrace();
		} catch (JSONException e) {
			Log.e(TAG, "JSONException: ", e);
			e.printStackTrace();
		}
	
		finally {
			if (con != null) {
			con.disconnect();
			}
		}
		// All done
		//Log.d(TAG, " -> returned " + result);
		return listItems;
	}
}
